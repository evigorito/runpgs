
<!-- README.md is generated from README.Rmd. Please edit that file -->

# runPGS

The goal of runPGS is to provide R funtions for [applyPGS
pipeline](https://gitlab.com/ukbb_covid19/applypgs)

## Installation

``` r

install.packages("devtools") # if you don't already have the package
install.packages( "http://www.well.ox.ac.uk/~gav/resources/rbgen_v1.1.4.tgz", repos = NULL, type = "source" )
library(devtools)
devtools::install_gitlab("evigorito/runpgs")
```
